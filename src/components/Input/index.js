import React from "react";
import PropType from "prop-types";

export default function Input({ type, value, name, onChange }) {
  return (
    <input type={type} name={name} value={value} onChange={onChange}></input>
  );
}

// //Memberikan nilai default
// Input.defaultProps = {
//   name: "name",
// };

//Menentukan tipe data
Input.PropType = {
  name: PropType.string.isRequired,
  onChange: PropType.func.isRequired,
};
